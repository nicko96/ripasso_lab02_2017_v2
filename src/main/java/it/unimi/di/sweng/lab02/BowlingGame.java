package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {

	private int score = 0;
	private int frames[] = new int[22];
	private int frame = 0;
	private int delivery = 0;
	
	@Override
	public void roll(int pins) {
		frames[frame] = pins;
		if(pins == 10 && delivery == 0 && frame < 20){					
			frame++;
			delivery++;
			frames[frame] = -1;			
		}
		delivery = (delivery+1)%2;
		frame++;
	}

	@Override
	public int score() {
		delivery = 0;
		for(int i=0; i<22; i++){
			if(frames[i] >= 0)
				score += frames[i];
			if(delivery == 1 && frames[i-1] == 10 && i < 18){
				int n = i+1;
				while(frames[n] < 0)
					n++;
				score += frames[n];
				n++;
				while(frames[n] < 0)
					n++;
				score += frames[n];
			}
			else if(delivery == 1 && frames[i]+frames[i-1] == 10 && i < 18)
				score += frames[i+1];
			delivery = (delivery+1)%2;
				}
		return score;
	}

}
